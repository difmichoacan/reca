﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReCA.Models;
using ReCA.Repositories;
using ReCA.Constants;

namespace ReCA.Controllers
{
    [Authorize(Roles=RolesConstants.Usuario)]
    public partial class RegistroPeriodoController : Controller
    {
        private EntitiesContext db = new EntitiesContext();

        //
        // GET: /RegistroPeriodo/

        public virtual ActionResult Index(int id)
        {
            PeriodoRepository periodoRepository = new PeriodoRepository(db);
            RegistroPeriodoRepository repository = new RegistroPeriodoRepository(db);
            List<RegistroPeriodo> registroPeriodos = repository.RegistroPeriodosForCadi(id);
            ViewBag.PeriodoNombre = periodoRepository.GetCurrentPeriod().Nombre;
            return View(registroPeriodos);
        }

        //
        // GET: /RegistroPeriodo/Details/5

        public virtual ActionResult Details(int id = 0)
        {
            RegistroPeriodo registroperiodo = db.RegistroPeriodoes.Find(id);
            if (registroperiodo == null)
            {
                return HttpNotFound();
            }
            return View(registroperiodo);
        }

        //
        // GET: /RegistroPeriodo/Create

        public virtual ActionResult Create(int id)
        {
            PeriodoRepository repository = new PeriodoRepository(db);
            RegistroPeriodoView registroPeriodoView = new RegistroPeriodoView()
            {
                Cadi=id,
                PeriodoNombre=repository.GetCurrentPeriod().Nombre
            };
            return View(registroPeriodoView);
        }

        //
        // POST: /RegistroPeriodo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(RegistroPeriodoView registroPeriodoView)
        {
            if (ModelState.IsValid)
            {
                RegistroPeriodoRepository repository = new RegistroPeriodoRepository(db);
                repository.Add(registroPeriodoView);
                return RedirectToAction(MVC.Cadi.Details(registroPeriodoView.Cadi));
            }

            return View(registroPeriodoView);
        }

        //
        // GET: /RegistroPeriodo/Edit/5

        public virtual ActionResult Edit(int id = 0)
        {
            RegistroPeriodoRepository repository = new RegistroPeriodoRepository(db);
            RegistroPeriodoView registroPeriodoView = repository.GetViewModel(id);
            return View(MVC.RegistroPeriodo.Views.Create, registroPeriodoView);
        }

        //
        // POST: /RegistroPeriodo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(RegistroPeriodoView registroperiodo)
        {
            if (ModelState.IsValid)
            {
                RegistroPeriodoRepository repository = new RegistroPeriodoRepository(db);
                repository.Save(registroperiodo);
                return RedirectToAction(MVC.Cadi.Details(registroperiodo.Cadi));
            }
            return View(registroperiodo);
        }

        //
        // GET: /RegistroPeriodo/Delete/5

        public virtual ActionResult Delete(int id = 0)
        {
            RegistroPeriodo registroperiodo = db.RegistroPeriodoes.Find(id);
            if (registroperiodo == null)
            {
                return HttpNotFound();
            }
            return View(registroperiodo);
        }

        //
        // POST: /RegistroPeriodo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {
            RegistroPeriodo registroperiodo = db.RegistroPeriodoes.Find(id);
            db.RegistroPeriodoes.Remove(registroperiodo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}