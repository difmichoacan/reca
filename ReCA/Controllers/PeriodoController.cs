﻿using ReCA.Constants;
using ReCA.Models;
using ReCA.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReCA.Controllers
{
    [Authorize(Roles=RolesConstants.Administrador)]
    public partial class PeriodoController : Controller
    {
        private EntitiesContext db = new EntitiesContext();

        //
        // GET: /Periodo/

        public virtual ActionResult Index()
        {
            PeriodoRepository repository = new PeriodoRepository(db);
            Periodo periodo = repository.GetCurrentPeriod();
            ViewBag.GeneratedPeriodNombre = PeriodoNombreFactory.GetNombre();
            return View(MVC.Periodo.Views.Index, periodo);
        }
        public virtual ActionResult OpenPeriodo()
        {
            PeriodoRepository repository = new PeriodoRepository(db);
            repository.OpenPeriod();
            return RedirectToAction(MVC.Periodo.Index());
        }
        public virtual ActionResult ClosePeriod()
        {
            PeriodoRepository repository = new PeriodoRepository(db);
            repository.PausePeriod();
            return RedirectToAction(MVC.Periodo.Index());
        }
        public virtual ActionResult ReopenPeriod()
        {
            PeriodoRepository repository = new PeriodoRepository(db);
            repository.ReopenPeriod();
            return RedirectToAction(MVC.Periodo.Index());
        }
        
    }
}
