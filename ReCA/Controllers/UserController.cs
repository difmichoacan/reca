﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReCA.Models;
using WebMatrix.WebData;
using System.Web.Security;
using ReCA.Constants;

namespace ReCA.Controllers
{
    [Authorize(Roles=RolesConstants.Administrador)]
    public partial class UserController : Controller
    {
        private EntitiesContext db = new EntitiesContext();

        //
        // GET: /User/

        public virtual ActionResult Index()
        {
            return View(db.UserProfiles.ToList());
        }


        //
        // GET: /User/Create

        public virtual ActionResult Create()
        {
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(RegisterModel userprofile)
        {
            if (ModelState.IsValid)
            {
                WebSecurity.CreateUserAndAccount(userprofile.UserName, userprofile.Password);
                Roles.AddUserToRole(userprofile.UserName, RolesConstants.Usuario);
                if (userprofile.IsAdmin)
                    Roles.AddUserToRole(userprofile.UserName, RolesConstants.Administrador);
                return RedirectToAction(MVC.User.Index());
            }

            return View(userprofile);
        }

        //
        // GET: /User/Edit/5

        public virtual ActionResult Edit(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }

            RegisterModel model = new RegisterModel()
            {
                UserName = userprofile.UserName
            };

            return View(model);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(RegisterModel userprofile)
        {
            if (ModelState.IsValid)
            {
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser user = Membership.GetUser(userprofile.UserName);
                    changePasswordSucceeded = user.ChangePassword(user.GetPassword(), userprofile.Password);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction(MVC.User.Index());
                }
                else
                {
                    ModelState.AddModelError("", "El nuevo password es inválido");
                }
            }
            return View(userprofile);
        }

        //
        // GET: /User/Delete/5

        public virtual ActionResult Delete(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            db.UserProfiles.Remove(userprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}