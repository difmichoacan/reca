﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReCA.Models;
using ReCA.Repositories;
using ReCA.Constants;
using AutoMapper;

namespace ReCA.Controllers
{
    [Authorize(Roles = RolesConstants.Usuario)]
    public partial class CadiController : Controller
    {
        private EntitiesContext db = new EntitiesContext();

        /// <summary>
        /// ïndice de los cadis, muestra la lista de cadis del usuario actual.
        /// </summary>
        /// <param name="resultsNumber">Número de resultados por página</param>
        /// <param name="page">Página que se debe mostrar</param>
        /// <param name="statusDisplay">Status de los cadis que se deben mostrar</param>
        /// <param name="orderBy">Orden en el que se deben mostrar los cadis</param>
        /// <returns>La vista del índice con la lista de cadis</returns>
        public virtual ActionResult Index(int resultsNumber = 10, int page = 0, CadiStatus statusDisplay = 0, CadiListOrder orderBy = CadiListOrder.Natural)
        {
            CadiRepository repository = new CadiRepository(db);
            CadiListView model = repository.CadiList(resultsNumber, page, statusDisplay, orderBy);
            ViewBag.Periodo = (new PeriodoRepository(db)).GetCurrentPeriod();
            return View(model);
        }

        //
        // GET: /Cadi/Details/5

        public virtual ActionResult Details(int id = 0)
        {
            CadiRepository repository = new CadiRepository(db);
            CadiDetailViewModel cadi = new CadiDetailViewModel()
            {
                Cadi = (Cadi)repository.GetInstanceById(id),
                Periodo=(new PeriodoRepository(db)).GetCurrentPeriod(),
                RegistroPeriodos=(new RegistroPeriodoRepository(db)).RegistroPeriodosForCadi(id),
                HasRegisteredCurrentPeriod = (new RegistroPeriodoRepository(db)).HasRegisteredCurrentPeriod(id)
            };
            return View(cadi);
        }

        //
        // GET: /Cadi/Create

        public virtual ActionResult Create()
        {
            MunicipioRepository municipiosRepository = new MunicipioRepository(db);
            PeriodoRepository periodoRepository = new PeriodoRepository(db);
            ViewBag.Periodo = periodoRepository.GetCurrentPeriod();
            return View(new CadiView()
            {
                Municipios=municipiosRepository.AllInstances()
            });
        }

        //
        // POST: /Cadi/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(CadiView cadiView)
        {
            CadiRepository repository = new CadiRepository(db);
            PeriodoRepository periodoRepository = new PeriodoRepository(db);
            ViewBag.Periodo = periodoRepository.GetCurrentPeriod();
            if (ModelState.IsValid)
            {
                repository.Add(cadiView);
                return RedirectToAction(MVC.Cadi.Index());
            }
            LocalidadesRepository localidadRepository = new LocalidadesRepository(db);
            cadiView.Localidades = localidadRepository.LocalidadesInMunicipio(cadiView.Municipio);
            return View(cadiView);
        }

        //
        // GET: /Cadi/Edit/5

        public virtual ActionResult Edit(int id = 0)
        {
            CadiRepository repository = new CadiRepository(db);
            CadiView cadiViewModel = repository.GetViewModel(id);
            LocalidadesRepository localidadRepository = new LocalidadesRepository(db);
            cadiViewModel.Localidades = localidadRepository.LocalidadesInMunicipio(cadiViewModel.Municipio);
            PeriodoRepository periodoRepository = new PeriodoRepository(db);
            ViewBag.Periodo = periodoRepository.GetCurrentPeriod();
            if (cadiViewModel == null)
            {
                return HttpNotFound();
            }
            return View(MVC.Cadi.Views.Create, cadiViewModel);
        }

        //
        // POST: /Cadi/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(CadiView cadiViewModel)
        {
            if (ModelState.IsValid)
            {
                CadiRepository repository = new CadiRepository(db);
                repository.Save(cadiViewModel);
                return RedirectToAction("Index");
            }

            LocalidadesRepository localidadRepository = new LocalidadesRepository(db);
            cadiViewModel.Localidades = localidadRepository.LocalidadesInMunicipio(cadiViewModel.Municipio);
            PeriodoRepository periodoRepository = new PeriodoRepository(db);
            ViewBag.Periodo = periodoRepository.GetCurrentPeriod();
            return View(MVC.Cadi.Views.Create, cadiViewModel);
        }

        //
        // GET: /Cadi/Delete/5

        public virtual ActionResult Delete(int id = 0)
        {
            Cadi cadimodels = db.Cadi.Find(id);
            if (cadimodels == null)
            {
                return HttpNotFound();
            }
            return View(cadimodels);
        }

        //
        // POST: /Cadi/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {
            Cadi cadimodels = db.Cadi.Find(id);
            db.Cadi.Remove(cadimodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public virtual ActionResult Ticket()
        {
            CadiRepository repository = new CadiRepository(this.db);
            CadiTicketView ticketView = repository.Ticket();
            return View(ticketView);
        }
        /// <summary>
        /// Da de baja el Centro indicado, regresa a la lista de Centros después
        /// </summary>
        /// <param name="id">El ID del centro para dar de baja</param>
        /// <param name="resultsNumber">Número de elementos a mostrar en la página.</param>
        /// <param name="page">Página a la que se debe saltar</param>
        /// <param name="statusDisplay">El status de los Centros que se deben Mostrar</param>
        /// <param name="orderBy">El orden de los elementos</param>
        /// <returns>Redirecciona a el listado de centros con los parámetros indicados</returns>
        public virtual ActionResult TakeDown(int id,int resultsNumber = 10, int page = 0, CadiStatus statusDisplay = 0, CadiListOrder orderBy = CadiListOrder.Natural)
        {
            CadiRepository repository = new CadiRepository(this.db);
            repository.TakeDown(id);
            return RedirectToAction(MVC.Cadi.Index(resultsNumber,page,statusDisplay,orderBy));
        }
    }
}