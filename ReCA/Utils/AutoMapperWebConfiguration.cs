﻿using AutoMapper;
using ReCA.Models;
namespace ReCA.Utils
{
    public class AutoMapperWebConfiguration
    {

        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new CadiProfile());
                cfg.AddProfile(new EntityToIdProfile());
                cfg.AddProfile(new RegistroPeriodoProfile());
            });
        }

    }

    class CadiProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Cadi, CadiView>()
                .ForMember(c=>c.Municipio,option=>option.MapFrom(c=>c.Localidad.Municipio.Id));
            Mapper.CreateMap<CadiView, Cadi>()
                .ForMember(c => c.Localidad, option => option.ResolveUsing<LocalidadResolver>());
        }
    }

    class EntityToIdProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Localidad, int>().ConvertUsing<LocalidadTypeConverter>();
        }
    }
    class RegistroPeriodoProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<RegistroPeriodo, RegistroPeriodoView>()
                .ForMember(r=>r.Cadi,option=>option.MapFrom(r=>r.Cadi.Id));
            Mapper.CreateMap<RegistroPeriodoView, RegistroPeriodo>()
                .ForMember(r=>r.Cadi,option=>option.ResolveUsing<CadiResolver>());
        }
    }

    class LocalidadTypeConverter : ITypeConverter<Localidad, int>
    {
        public int Convert(ResolutionContext context)
        {
            Localidad localidad = (Localidad)context.SourceValue;
            if (localidad == null) return 0;
            return localidad.Id;
        }
    }

    class LocalidadResolver : ValueResolver<IHasLocalidad, Localidad>
    {
        protected override Localidad ResolveCore(IHasLocalidad source)
        {
            if (source.Localidad == 0)
                return null;
            return (Localidad)source.LocalidadRepository.GetInstanceById(source.Localidad);
        }
    }
    class CadiResolver : ValueResolver<IHasCadi, Cadi>
    {
        protected override Cadi ResolveCore(IHasCadi source)
        {
            if (source.Cadi==0)
                return null;
            return (Cadi)source.CadiRepository.GetInstanceById(source.Cadi);
        }
    }
}
