﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace ReCA.Models
{
    public class RegistroPeriodo
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public virtual Cadi Cadi { get; set; }

        public int LactantesHombres { get; set; }

        public int LactantesMujeres { get; set; }

        public int MaternalesHombres { get; set; }

        public int MaternalesMujeres { get; set; }

        public int PreescolaresHombres { get; set; }

        public int PreescolaresMujeres { get; set; }

        public int TotalFamiliasBeneficiadas { get; set; }


        public virtual Periodo Periodo { get; set; }
    }

    public class RegistroPeriodoView : IHasCadi
    {
        public int Id { get; set; }

        public int Cadi { get; set; }

        public int LactantesHombres { get; set; }

        public int LactantesMujeres { get; set; }

        public int MaternalesHombres { get; set; }

        public int MaternalesMujeres { get; set; }

        public int PreescolaresHombres { get; set; }

        public int PreescolaresMujeres { get; set; }

        [NotMapped]
        public IRepository CadiRepository { get; set; }

        public string PeriodoNombre { get; set; }
        [Display(Name="Total de familias beneficiadas")]
        public int TotalFamiliasBeneficiadas { get; set; }
    }
}
