﻿    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReCA.Models
{
    public enum Turno 
    {
        [Description("Matutino")]
        Vespertino=1,
        [Description("Vespertino")]
        Matutino,
        [Description("Ambos")]
        Ambos
    }
    public enum Modalidad
    {
        [Description("Público")]
        Publico =1,
        [Description("Privado")]
        Privado,
        [Description("Mixto")]
        Mixto
    }
    public enum Capacidad
    {
        [Description("10 o menos")]
        Tipo1=1,
        [Description("11 a 50")]
        Tipo2,
        [Description("51 a 100")]
        Tipo3,
        [Description("Más de 100")]
        Tipo4
    }
    public enum TipoPersona
    {
        [Description("Persona Física")]
        Fisica=1,
        [Description("Persona Moral")]
        Moral
    }
    public enum CadiStatus
    {
        Activo=1,
        Baja
    }
    public enum CadiListOrder
    {
        Natural = 1,
        StatusCaptured,
        StatusNotCaputred,
        StatusActive,
        StatusDown
    }
    public class Cadi
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name="Clave")]
        public string Clave { get; set; }

        [Display(Name="Persona")]
        public int TipoPersona { get; set; }

        [Display(Name="Responsable")]
        public string NombreResponsable { get; set; }

        [Display(Name="Localidad")]
        public virtual Localidad Localidad { get; set; }

        public string Nombre { get; set; }

        public string Domicilio { get; set; }

        public int Turno { get; set; }

        public int Modalidad { get; set; }

        public int Capacidad { get; set; }

        // TODO: En el articulo 37, en el punto VI, menciona la Capacidad instalada,
        // se entiende que viene definida por la propiedad Capacidad Capacidad, pero si se trata
        // de otra propiedad aparte con una precisión unitaria usar la siguiente propiedad:
        //public int CapacidadInstalada { get; set; }

        [Display(Name="Inicio de Operac.")]
        public DateTime FechaInicioOperaciones { get; set; }

        [Display(Name="Teléfono fijo")]
        public string TelefonoFijo { get; set; }

        [Display(Name="Teléfono Celular")]
        public string TelefonoCelular { get; set; }

        [Display(Name="Teléfono (Otro)")]
        public string TelefonoOtro { get; set; }

        public virtual UserProfile User { get; set; }
        
        public CadiStatus Status { get; set; }

        public DateTime TakeDownDate { get; set; }

        public Cadi()
        {
            this.TakeDownDate = DateTime.Now;
        }
    }

    public class CadiView : IHasLocalidad
    {
        public int Id { get; set; }
        
        [Required (ErrorMessage="Escriba la clave del centro")]
        [Display(Name="Clave")]
        public string Clave { get; set; }

        [Required (ErrorMessage="Escriba el nombre del responsable.")]
        [Display(Name="Nombre del Responsable")]
        public string NombreResponsable { get; set; }

        [Required (ErrorMessage="Escriba el nombre del centro")]
        [Display(Name="Nombre del Centro")]
        public string Nombre { get; set; }

        [Required (ErrorMessage="Escriba el domicilio")]
        [Display(Name = "Domicilio")]
        public string Domicilio { get; set; }

        [Required (ErrorMessage="Escriba la fecha de inicio de operaciones")]
        [Display(Name = "Fecha de Inicio de Operaciones")]
        [DisplayFormat(ApplyFormatInEditMode=true,DataFormatString="{0:dd/MM/yyyy}")]
        public DateTime FechaInicioOperaciones { get; set; }

        [Display(Name = "Teléfono Fijo")]
        public string TelefonoFijo { get; set; }

        [Display(Name = "Teléfono Celular")]
        public string TelefonoCelular { get; set; }

        [Display(Name = "Otro Teléfono")]
        public string TelefonoOtro { get; set; }

        [Display(Name = "Tipo de Persona")]
        public TipoPersona TipoPersona { get; set; }

        [Required (ErrorMessage="Seleccione una localidad")]
        public int Localidad { get; set; }

        public Turno Turno { get; set; }

        [Required (ErrorMessage="Seleccione la capacidad")]
        public Capacidad Capacidad { get; set; }

        public Modalidad Modalidad { get; set; }

        [NotMapped]
        public IRepository LocalidadRepository { get; set; }
        [NotMapped]
        public List<Municipio> Municipios { get; set; }
        [NotMapped]
        public int Municipio { get; set; }
        [NotMapped]
        public List<Localidad> Localidades { get; set; }
        
        public CadiStatus Status { get; set; }
    }

    public class CadiDetailViewModel
    {
        public Cadi Cadi { get; set; }
        public Periodo Periodo { get; set; }
        public List<RegistroPeriodo> RegistroPeriodos { get; set; }
        public bool HasRegisteredCurrentPeriod { get; set; }
    }

    public class CadiListElement
    {
        public Cadi Cadi { get; set; }

        public bool HasCapturedCurrentPeriodo { get; set; }
    }

    public class CadiTicketView
    {
        public string UnencodedString { get; set; }

        public int Sum { get; set; }

        public string EncodedString { get; set; }

        public int Captured { get; set; }

        public int Total { get; set; }

        public Periodo Periodo { get; set; }
    }

    public class CadiListView
    {
        public List<CadiListElement> CadiList { get; set; }

        public CadiStatus Display { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public int Total { get; set; }

        public CadiListOrder OrderBy { get; set; }
    }
}
