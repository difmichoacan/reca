﻿using System.Data.Entity;

namespace ReCA.Models
{
    public class EntitiesContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<ReCA.Models.EntitiesContext>());

        public EntitiesContext() : base("name=EntitiesContext")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Cadi> Cadi { get; set; }

        public DbSet<Municipio> Municipios { get; set; }

        public DbSet<Localidad> Localidades { get; set; }

        public DbSet<Periodo> Periodos { get; set; }

        public DbSet<RegistroPeriodo> RegistroPeriodoes { get; set; }
    }
}
