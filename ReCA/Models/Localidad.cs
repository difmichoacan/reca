﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace ReCA.Models
{
    public class Localidad
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // TODO: En caso de requerir una clave externa asignada manualmente agregar la propiedad:
        //public int/string Clave { get; set; }

        public virtual Municipio Municipio { get; set; }

        public string Nombre { get; set; }

    }
}
