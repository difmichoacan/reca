﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace ReCA.Models
{
    public enum PeriodoStatus
    {
        Abierto = 1,
        Pausado,
        Cerrado
    }
    public class PeriodoNombreFactory
    {

        internal static string GetNombre()
        {
            return DateTime.Now.Year + "-" + (DateTime.Now.Month <= 6 ? "1" : "2");
        }
    }
    public class Periodo
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int Status { get; set; }

        public string Nombre { get; set; }
    }
}
