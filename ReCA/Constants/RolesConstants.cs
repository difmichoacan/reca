﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReCA.Constants
{
    public class RolesConstants
    {
        public const string Administrador = "Administrador";
        public const string Usuario = "Usuario";
    }
}