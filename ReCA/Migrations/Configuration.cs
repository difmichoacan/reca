namespace ReCA.Migrations
{
    using ReCA.Constants;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<ReCA.Models.EntitiesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ReCA.Models.EntitiesContext context)
        {

            if (!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection(ConnectionStrings.DataBaseConnectionName, "UserProfile", "UserId", "UserName", autoCreateTables: true);

            if (!Roles.RoleExists(RolesConstants.Administrador))
                Roles.CreateRole(RolesConstants.Administrador);
            if (!Roles.RoleExists(RolesConstants.Usuario))
                Roles.CreateRole(RolesConstants.Usuario);

            string admin="Admin";
            if (!WebSecurity.UserExists(admin))
                WebSecurity.CreateUserAndAccount(admin, "nimdA", false);
            if (!Roles.IsUserInRole(admin, RolesConstants.Administrador))
                Roles.AddUserToRoles(admin, new string[] { RolesConstants.Administrador, RolesConstants.Usuario });
        }
    }
}
