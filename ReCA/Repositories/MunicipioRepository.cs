﻿using ReCA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReCA.Repositories
{
    public class MunicipioRepository
    {
        private EntitiesContext db;

        public MunicipioRepository(EntitiesContext db)
        {
            this.db = db;
        }

        internal List<Municipio> AllInstances()
        {
            return this.db.Municipios.OrderBy(m=>m.Nombre).ToList();
        }
    }
}
