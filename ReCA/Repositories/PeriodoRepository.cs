﻿using ReCA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReCA.Repositories
{
    class PeriodoRepository
    {
        private EntitiesContext db;

        public PeriodoRepository(EntitiesContext db)
        {
            this.db = db;
        }
        /// <summary>
        /// Obtiene el periodo actual de operación de todo el sistema, aquel que no haya sido cerrado.
        /// </summary>
        /// <returns>El Periodo actual sobre el cual opera el sistema</returns>
        public Periodo GetCurrentPeriod()
        {
            return this.db.Periodos.SingleOrDefault(p=>p.Status != (int)PeriodoStatus.Cerrado);
        }

        /// <summary>
        /// Abre el periodo de captura siguiente.
        /// </summary>
        internal void OpenPeriod()
        {
            Periodo past = this.GetCurrentPeriod();
            if (past != null)
            {
                past.Status = (int)PeriodoStatus.Cerrado;
            }
            this.db.Periodos.Add(new Periodo()
            {
                Nombre = PeriodoNombreFactory.GetNombre(),
                Status = (int)PeriodoStatus.Abierto
            });
            this.db.SaveChanges();
        }

        /// <summary>
        /// Cierra las capturas del periodo actual, pero con la opción de reabrirlo.
        /// </summary>
        internal void PausePeriod()
        {
            Periodo current = this.GetCurrentPeriod();
            if (current != null)
            {
                current.Status = (int)PeriodoStatus.Pausado;
            }
            this.db.SaveChanges();
        }

        /// <summary>
        /// Reabre el periodo actual.
        /// </summary>
        internal void ReopenPeriod()
        {
            Periodo current = this.GetCurrentPeriod();
            if (current != null)
            {
                current.Status = (int)PeriodoStatus.Abierto;
            }
            this.db.SaveChanges();
        }
    }
}
