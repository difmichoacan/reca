﻿using ReCA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReCA.Repositories
{
    class LocalidadesRepository : IRepository
    {
        private Models.EntitiesContext db;

        public LocalidadesRepository(Models.EntitiesContext db)
        {
            this.db = db;
        }
        internal List<Models.Localidad> LocalidadesInMunicipio(int p)
        {
            return this.db.Localidades
                .Where(l => l.Municipio.Id == p)
                .OrderBy(l=>l.Nombre)
                .ToList();
        }

        public EntitiesContext Context { get; set; }
        public object GetInstanceById(int id)
        {
            return this.db.Localidades.SingleOrDefault(l => l.Id == id);
        }
    }
}
