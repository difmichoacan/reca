﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReCA.Repositories
{
    class UserRepository
    {
        private Models.EntitiesContext entitiesContext;

        public UserRepository(Models.EntitiesContext entitiesContext)
        {
            this.entitiesContext = entitiesContext;
        }

        /// <summary>
        /// Obtiene la instancia del UserProfile con el nombre de usuario que se indica.
        /// </summary>
        /// <param name="userName">El nombre del usuario que se buscará.</param>
        /// <returns>Una instancia de UserProfile que coincide con el nombre de usuario, o null si no se encontró la coincidencia.</returns>
        internal Models.UserProfile UserForName(string userName)
        {
            return this.entitiesContext.UserProfiles.SingleOrDefault(u => u.UserName.Equals(userName));
        }
    }
}
