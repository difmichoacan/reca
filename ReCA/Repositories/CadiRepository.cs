﻿using AutoMapper;
using ReCA.Constants;
using ReCA.Models;
using ReCA.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;

namespace ReCA.Repositories
{
    class CadiRepository : IRepository
    {
        public CadiRepository(EntitiesContext context)
        {
            this.Context = context;
        }
        public EntitiesContext Context { get; set; }
        public object GetInstanceById(int id)
        {
            return this.Context.Cadi
                .SingleOrDefault(c => c.Id == id);
        }

        /// <summary>
        /// Agrega una nueva instanca al repositorio de Cadi
        /// </summary>
        /// <param name="cadiView">Una instancia del modelo de la vista que será convertido al modelo de la base de datos y almacenado</param>
        /// <returns>Verdadero si no ocurrió un error</returns>
        internal bool Add(CadiView cadiView)
        {
            UserRepository userRepository = new UserRepository(this.Context);
            UserProfile user = userRepository.UserForName(HttpContext.Current.User.Identity.Name);
            cadiView.LocalidadRepository = new LocalidadesRepository(this.Context);
            Cadi cadi = Mapper.Map<Cadi>(cadiView);
            cadi.User = user;
            cadi.Status = CadiStatus.Activo;
            this.Context.Cadi.Add(cadi);
            this.Context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Obtiene una lista de CADIs.
        /// </summary>
        /// <param name="pageSize">El número de resultados máximo que serán devueltos </param>
        /// <param name="page">La página que debe ser devuelta.</param>
        /// <param name="display"></param>
        /// <param name="orderBy"></param>
        /// <returns>Devuelve una lista de CadiListElement relacionados al usuario de la sesión actual y en el periodo actual.</returns>
        internal CadiListView CadiList(int pageSize=0,int page=0, CadiStatus display=0, CadiListOrder orderBy=CadiListOrder.Natural)
        {
            UserRepository userRepository = new UserRepository(this.Context);
            UserProfile user = userRepository.UserForName(HttpContext.Current.User.Identity.Name);
            Periodo currentPeriodo = new PeriodoRepository(this.Context).GetCurrentPeriod();

            IQueryable<CadiListElement> results = from c in this.Context.Cadi
                                                  let r = this.Context.RegistroPeriodoes.Where(registro => registro.Periodo.Id == currentPeriodo.Id && registro.Cadi.Id == c.Id)
                                                  select new CadiListElement()
                                                  {
                                                      Cadi = c,
                                                      HasCapturedCurrentPeriodo = r.Count() > 0
                                                  };
            switch (orderBy)
            {
                case CadiListOrder.Natural:
                    results = results.OrderBy(c => c.Cadi.Clave);
                    break;
                case CadiListOrder.StatusCaptured:
                    results = results.OrderByDescending(c => c.Cadi.Status == CadiStatus.Activo).ThenBy(c => !c.HasCapturedCurrentPeriodo);
                    break;
                case CadiListOrder.StatusNotCaputred:
                    results = results.OrderByDescending(c => c.Cadi.Status == CadiStatus.Activo).ThenBy(c => c.HasCapturedCurrentPeriodo);
                    break;
                case CadiListOrder.StatusActive:
                    results = results.OrderByDescending(c => c.Cadi.Status==CadiStatus.Activo).ThenBy(c=>c.Cadi.Status);
                    break;
                case CadiListOrder.StatusDown:
                    results = results.OrderByDescending(c => c.Cadi.Status == CadiStatus.Baja).ThenBy(c => c.Cadi.Status);
                    break;
            }
            
            if (display != 0)
            {
                results = results.Where(c => c.Cadi.Status == display);
            }
            if (!Roles.IsUserInRole(user.UserName, RolesConstants.Administrador))
            {
                results = results.Where(c => c.Cadi.User.UserId == user.UserId);
            }

            results = results.Skip(page * pageSize).Take(pageSize);

            int total = results.Count();
            return new CadiListView()
            {
                CadiList = results.ToList(),
                Page = page,
                PageSize = pageSize,
                Display = display,
                Total = total,
                OrderBy = orderBy
            };
        }

        /// <summary>
        /// Obtiene una instancia del modelo de la vista para el id indicado
        /// </summary>
        /// <param name="id">El id de la instancia que se busca</param>
        /// <returns>Una instancia de CadiView, el modelo para la vista con el id indicado.</returns>
        internal CadiView GetViewModel(int id)
        {
            return Mapper.Map<CadiView>(this.GetInstanceById(id));
        }

        /// <summary>
        /// Guarda los cambios realizados sobre el modelo de la vista.
        /// </summary>
        /// <param name="cadiViewModel">El modelo de la vista que contiene los cambios a realizar</param>
        internal void Save(CadiView cadiViewModel)
        {
            Cadi existingInstance = (Cadi)this.GetInstanceById(cadiViewModel.Id);
            cadiViewModel.LocalidadRepository = new LocalidadesRepository(this.Context);
            Mapper.Map<CadiView, Cadi>(cadiViewModel, existingInstance);
            existingInstance.Status = CadiStatus.Activo;
            this.Context.SaveChanges();
        }
        /// <summary>
        /// Genera una instancia del modelo para la vista del comprobante de captura.
        /// El comprobante es válido para el usuario actual y el periodo actual.
        /// El método toma en cuenta la fecha, el número de registros realizados, el usuario que ha realizado el registro
        /// Se usa un Hash SHA1 para encriptar los datos.
        /// </summary>
        /// <returns>Una instancia de CadiTicketView, para usarse en la vista del comprobante </returns>
        internal CadiTicketView Ticket()
        {
            CadiListView cadiListView = this.CadiList();
            string unencodedString = "";
            int sum = 0;
            int count = 0;
            foreach (CadiListElement cadiListElement in cadiListView.CadiList)
            {
                unencodedString += cadiListElement.Cadi.Id + (cadiListElement.HasCapturedCurrentPeriodo ? "-" : ".");
                if (cadiListElement.HasCapturedCurrentPeriodo) count++;
                sum += cadiListElement.Cadi.Id;
            }
            Periodo periodo =  new PeriodoRepository(this.Context).GetCurrentPeriod();
            DateTime now = DateTime.Now;
            var bytes = Encoding.UTF8.GetBytes(periodo.Id + HttpContext.Current.User.Identity.Name + now.DayOfYear + unencodedString + sum);
            SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();

            return new CadiTicketView()
            {
                Captured = count,
                Total = cadiListView.CadiList.Count,
                UnencodedString = unencodedString,
                Sum = sum,
                Periodo =periodo,
                EncodedString = BitConverter.ToString(cryptoTransformSHA1.ComputeHash(bytes)).Replace("-", "")
            };
        }

        /// <summary>
        /// Da de baja la instancia de Cadi que tenga el id indicado.
        /// </summary>
        /// <param name="id">El id del Cadi que será dado de baja.</param>
        internal void TakeDown(int id)
        {
            Cadi cadi = (Cadi)this.GetInstanceById(id);
            cadi.Status = CadiStatus.Baja;
            cadi.TakeDownDate = DateTime.Now;
            this.Context.SaveChanges();
        }
    }
}
