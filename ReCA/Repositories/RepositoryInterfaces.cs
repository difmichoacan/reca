﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReCA.Models
{
    public interface IRepository
    {
        EntitiesContext Context { get; set; }

        /// <summary>
        /// Obtiene la instancia de la entidad con el id que se indica
        /// </summary>
        /// <param name="id">El id de la instancia que se busca</param>
        /// <returns>La instancia con id que se indicó</returns>
        object GetInstanceById(int id);
    }
    public interface IHasLocalidad
    {
        IRepository LocalidadRepository { get; set; }
        int Localidad { get; set; }
    }
    public interface IHasCadi
    {
        IRepository CadiRepository { get; set; }
        int Cadi { get; set; }
    }
}
