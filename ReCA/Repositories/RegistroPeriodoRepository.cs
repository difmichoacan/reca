﻿using AutoMapper;
using ReCA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReCA.Repositories
{
    public class RegistroPeriodoRepository : IRepository
    {
        public EntitiesContext Context { get; set; }

        public RegistroPeriodoRepository(EntitiesContext db)
        {
            this.Context = db;
        }
        /// <summary>
        /// Obtiene la lista de registros para el cadi con el id indicado.
        /// </summary>
        /// <param name="id">El Id del Cadi del que se obtendrán los Registros.</param>
        /// <returns></returns>
        internal List<RegistroPeriodo> RegistroPeriodosForCadi(int id)
        {
            return this.Context.RegistroPeriodoes
                .Where(r => r.Cadi.Id == id)
                .ToList();
        }

        /// <summary>
        /// Agrega un nuevo registro desde el modelo de la vista.
        /// </summary>
        /// <param name="registroPeriodoView">El modelo de la vista que contiene los datos que serán almacenados.</param>
        internal void Add(RegistroPeriodoView registroPeriodoView)
        {
            PeriodoRepository periodoRepository = new PeriodoRepository(Context);
            registroPeriodoView.CadiRepository = new CadiRepository(Context);
            RegistroPeriodo registroPeriodo = Mapper.Map<RegistroPeriodo>(registroPeriodoView);
            registroPeriodo.Periodo = periodoRepository.GetCurrentPeriod();

            this.Context.RegistroPeriodoes.Add(registroPeriodo);
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Indica si se ha registrado el periodo actual para el Cadi con el id indicado.
        /// </summary>
        /// <param name="id">El Id del cadi del cual se quiere saber si ya se ha registrado el periodo</param>
        /// <returns>Verdadero si ya se ha registrado el periodo, falso de otra manera.</returns>
        internal bool HasRegisteredCurrentPeriod(int id)
        {
            PeriodoRepository periodoRepository = new PeriodoRepository(Context);
            Periodo currentPeriodo = periodoRepository.GetCurrentPeriod();

            return this.Context.RegistroPeriodoes.Where(r => r.Cadi.Id == id)
                .SingleOrDefault(r => r.Periodo.Id == currentPeriodo.Id) != null;
        }

        public object GetInstanceById(int id)
        {
            return this.Context.RegistroPeriodoes.Include("Cadi").SingleOrDefault(r => r.Id == id);
        }

        /// <summary>
        /// Obtiene el modelo para usar en las vistas de registro del periodo.
        /// </summary>
        /// <param name="id">El id del registro que será mostrado.</param>
        /// <returns>Una instancia de RegistroPeriodoView con los datos que corresponden al id proporcionado.</returns>
        internal RegistroPeriodoView GetViewModel(int id)
        {
            RegistroPeriodo registroPeriodo = (RegistroPeriodo) this.GetInstanceById(id);
            RegistroPeriodoView registroPeriodoView = Mapper.Map<RegistroPeriodoView>(registroPeriodo);

            return registroPeriodoView;
        }

        /// <summary>
        /// Guarda los datos del registro desde un modelo de vista.
        /// </summary>
        /// <param name="registroPeriodoView">El modelo de la vista que contiene los cambios a los datos que serán guardados.</param>
        internal void Save(RegistroPeriodoView registroPeriodoView)
        {
            RegistroPeriodo registroPeriodo = (RegistroPeriodo) this.GetInstanceById(registroPeriodoView.Id);
            registroPeriodoView.CadiRepository = new CadiRepository(this.Context);
            Mapper.Map<RegistroPeriodoView,RegistroPeriodo>(registroPeriodoView,registroPeriodo);
            this.Context.SaveChanges();
        }
    }
}
